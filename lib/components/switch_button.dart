import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:tomisha_test/provider.dart';

class GroupToggleButton extends ConsumerWidget {
  const GroupToggleButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final feature = ref.watch(featureProvider);
    List<bool> selectList = [];
    feature.when(
      displayOne: () => (selectList = [true, false, false]),
      displayTwo: () => (selectList = [false, true, false]),
      displayThree: () => (selectList = [false, false, true]),
    );
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: [
        ToggleButtons(
          color: const Color(0xff319795),
          constraints: const BoxConstraints(minWidth: 160, minHeight: 40),
          borderColor: const Color(0xffCBD5E0),
          borderRadius: BorderRadius.circular(12),
          selectedColor: const Color(0xffE6FFFA),
          focusColor: const Color(0xff81E6D9),
          // hoverColor: const Color(0xffE6FFFA),
          selectedBorderColor: const Color(0xff81E6D9),
          highlightColor: const Color(0xff81E6D9),
          fillColor: const Color(0xff81E6D9),
          onPressed: (int index) {
            final notifier = ref.read(featureProvider.notifier);
            notifier.toggleFeature(index);
          },
          isSelected: selectList,
          children: const <Widget>[
            Center(child: Text('Arbietnehmer')),
            Center(child: Text('Arbietgeber')),
            Center(child: Text('Temporarburo')),
          ],
        ),
      ],
    );
  }
}
