import 'package:flutter/material.dart';
import 'package:tomisha_test/components/app_bar.dart';
import 'package:tomisha_test/components/feature.dart';
import 'package:tomisha_test/components/home_banner.dart';
import 'package:tomisha_test/components/switch_button.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tomisha_test/components/text_heading.dart';

class DesktopHomePage extends StatefulWidget {
  const DesktopHomePage({
    Key? key,
  }) : super(key: key);

  @override
  State<DesktopHomePage> createState() => _DesktopHomePageState();
}

class _DesktopHomePageState extends State<DesktopHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: AppBar().preferredSize,
        child: const CustomeAppBar(),
      ),
      body: ListView(
        children: [
          const HomeBanner(),
          const GroupToggleButton(),
          SizedBox(
            height: 30.h,
          ),
          const TextBanner(),
          SizedBox(
            height: 30.h,
          ),
          const FeatureDisplay(),
        ],
      ),
    );
  }
}
