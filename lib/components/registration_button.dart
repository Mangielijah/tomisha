import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:tomisha_test/provider.dart';
import 'package:visibility_detector/visibility_detector.dart';

class RegistrationButton extends ConsumerWidget {
  const RegistrationButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return VisibilityDetector(
      key: const Key('RegistrationButton'),
      onVisibilityChanged: (VisibilityInfo info) {
        var visiblePercentage = info.visibleFraction * 100;
        final btnState = ref.read(buttonStatusProvider);
        final notifier = ref.read(buttonStatusProvider.notifier);
        btnState.when(visible: () {
          if (visiblePercentage > 50) {
            notifier.toggleVisibility();
          }
        }, notVisible: () {
          if (visiblePercentage < 50) {
            notifier.toggleVisibility();
          }
        });
      },
      child: ElevatedButton(
        onPressed: () {},
        style: ElevatedButton.styleFrom(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
          padding: const EdgeInsets.all(0.0),
        ),
        child: Ink(
          decoration: const BoxDecoration(
            gradient: LinearGradient(colors: [
              Color(0xff319795),
              Color(0xff3182CE),
            ]),
            borderRadius: BorderRadius.all(Radius.circular(12.0)),
          ),
          child: Container(
            constraints: const BoxConstraints(
              minHeight: 36.0,
            ),
            alignment: Alignment.center,
            child: const Text(
              'Kostenlos Registrieren',
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
    );
  }
}
