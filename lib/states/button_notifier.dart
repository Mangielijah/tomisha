import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:tomisha_test/states/button_state.dart';

class ButtonWidgetNotifier extends StateNotifier<RegButtonState> {
  ButtonWidgetNotifier() : super(const RegButtonState.notVisible());

  void toggleVisibility() {
    state.when(visible: () {
      state = const RegButtonState.notVisible();
    }, notVisible: () {
      state = const RegButtonState.visible();
    });
  }
}
