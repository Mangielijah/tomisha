import 'package:freezed_annotation/freezed_annotation.dart';

part 'button_state.freezed.dart';

@freezed
class RegButtonState with _$RegButtonState {
  const factory RegButtonState.visible() = VisibleBTN;
  const factory RegButtonState.notVisible() = InVisibleBTN;
}
