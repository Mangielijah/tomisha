import 'package:flutter/material.dart';
import 'package:tomisha_test/components/app_bar.dart';
import 'package:tomisha_test/components/mobile_feature.dart';
import 'package:tomisha_test/components/mobile_home_banner.dart';
import 'package:tomisha_test/components/registration_button.dart';
import 'package:tomisha_test/components/switch_button.dart';
import 'package:tomisha_test/components/text_heading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MobileHomePage extends StatefulWidget {
  const MobileHomePage({
    Key? key,
  }) : super(key: key);

  @override
  State<MobileHomePage> createState() => _MobileHomePageState();
}

class _MobileHomePageState extends State<MobileHomePage> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: AppBar().preferredSize,
          child: const CustomeAppBar(),
        ),
        body: SingleChildScrollView(
            child: SizedBox(
          height: (size.height - AppBar().preferredSize.height) + 1300,
          width: size.width,
          child: Stack(
            children: [
              const MobileHomeBanner(),
              Positioned(
                top: 540,
                left: 0,
                child: Column(
                  children: [
                    Container(
                      height: 100,
                      width: size.width,
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(12),
                          topRight: Radius.circular(12),
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey[300]!,
                            blurRadius: 15.0,
                            offset: const Offset(0.0, 0),
                            blurStyle: BlurStyle.solid,
                          ),
                        ],
                        color: Colors.white,
                      ),
                      child: Center(
                        child: Container(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          height: 50,
                          child: const RegistrationButton(),
                        ),
                      ),
                    ),
                    Container(
                      color: Colors.white,
                      width: size.width,
                      // height: 100,
                      child: Column(
                        children: [
                          SizedBox(
                            height: 50,
                            width: size.width,
                            // padding: const EdgeInsets.symmetric(horizontal: 20),
                            child: const SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              padding: EdgeInsets.symmetric(horizontal: 20),
                              primary: true,
                              child: GroupToggleButton(),
                            ),
                          ),
                          SizedBox(
                            height: 30.h,
                          ),
                          const TextBanner(),
                          SizedBox(
                            height: 30.h,
                          ),
                          const MobileFeatureDisplay()
                        ],
                      ),
                    )
                    // GroupToggleButton(),
                    // SizedBox(
                    //   height: 30.h,
                    // ),
                    // TextBanner(),
                    // SizedBox(
                    //   height: 30.h,
                    // ),
                    // FeatureDisplay(),
                  ],
                ),
              ),
            ],
          ),
        )),
      ),
    );
  }
}
