import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tomisha_test/utils.dart';

class FeatureDisplay extends StatelessWidget {
  const FeatureDisplay({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 1350,
      child: Stack(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: 20.h,
              ),
              const FeatureOne(),
              SizedBox(
                height: 80.h,
              ),
              const FeatureTwo(),
              SizedBox(
                height: 80.h,
              ),
              const FeatureThree(),
            ],
          ),
          Positioned(
            top: 250,
            left: 400,
            child: SvgPicture.asset(
              Assets.downArrowRight,
              height: 387,
              fit: BoxFit.cover,
              alignment: Alignment.topCenter,
            ),
          ),
          Positioned(
            bottom: 390,
            left: 450,
            child: SvgPicture.asset(
              Assets.downArrowLeft,
              height: 234,
              fit: BoxFit.cover,
              alignment: Alignment.topCenter,
            ),
          ),
        ],
      ),
    );
  }
}

class FeatureOne extends StatelessWidget {
  const FeatureOne({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SizedBox(
      // color: Colors.grey,
      height: 253,
      child: Stack(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                width: size.width * 0.205,
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: Container(
                  margin: const EdgeInsets.only(bottom: 12),
                  child: const CircleAvatar(
                    radius: 62,
                    backgroundColor: Color(0xffF7FAFC),
                  ),
                ),
              ),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                width: size.width * 0.223,
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: Container(
                  margin: const EdgeInsets.only(bottom: 24),
                  child: Text(
                    '1.',
                    style: TextStyle(
                      fontSize: 65.sp,
                      color: const Color(0xff718096),
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: Container(
                  margin: const EdgeInsets.only(bottom: 42),
                  child: Text(
                    'Erstellen dein Lebenslauf',
                    style: TextStyle(
                      fontWeight: FontWeight.normal,
                      fontSize: 15.sp,
                      color: const Color(0xff718096),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                width: 36,
              ),
              SvgPicture.asset(
                Assets.featureOne,
                height: 253,
                fit: BoxFit.cover,
                alignment: Alignment.topCenter,
              ),
            ],
          )
        ],
      ),
    );
  }
}

class FeatureTwo extends StatelessWidget {
  const FeatureTwo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SizedBox(
      // color: Colors.grey,
      height: 370,
      child: Stack(
        children: [
          Align(
            alignment: Alignment.topLeft,
            child: ClipPath(
              clipper: WaveClipperOne(
                reverse: true,
              ),
              child: Container(
                height: 185,
                width: double.infinity,
                decoration: const BoxDecoration(
                    gradient: LinearGradient(
                  colors: [
                    Color(0xffE6FFFA),
                    Color(0xffEBF4FF),
                  ],
                )),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomLeft,
            child: ClipPath(
              clipper: WaveClipperOne(),
              child: Container(
                height: 185,
                width: double.infinity,
                decoration: const BoxDecoration(
                    gradient: LinearGradient(
                  colors: [
                    Color(0xffE6FFFA),
                    Color(0xffEBF4FF),
                  ],
                )),
              ),
            ),
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                width: size.width * 0.25,
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: Container(
                  margin: const EdgeInsets.only(bottom: 42),
                  child: SvgPicture.asset(
                    Assets.featureTwo,
                    height: 227,
                    fit: BoxFit.cover,
                    alignment: Alignment.topCenter,
                  ),
                ),
              ),
              const SizedBox(
                width: 65,
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: Container(
                  margin: const EdgeInsets.only(bottom: 60),
                  child: Text(
                    '2.',
                    style: TextStyle(
                      fontSize: 65.sp,
                      color: const Color(0xff718096),
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: Container(
                  margin: const EdgeInsets.only(bottom: 84),
                  child: Text(
                    'Erstellen dein Lebenslauf',
                    style: TextStyle(
                      fontWeight: FontWeight.normal,
                      fontSize: 15.sp,
                      color: const Color(0xff718096),
                    ),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

class FeatureThree extends StatelessWidget {
  const FeatureThree({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SizedBox(
      // color: Colors.grey,
      height: 376,
      child: Stack(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                width: size.width * 0.205,
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  margin: const EdgeInsets.only(top: 24),
                  child: const CircleAvatar(
                    radius: 120,
                    backgroundColor: Color(0xffF7FAFC),
                  ),
                ),
              ),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                width: size.width * 0.21,
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  margin: const EdgeInsets.only(top: 24),
                  child: Text(
                    '3.',
                    style: TextStyle(
                      fontSize: 65.sp,
                      color: const Color(0xff718096),
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  margin: const EdgeInsets.only(top: 62),
                  width: 275,
                  child: Text(
                    'Mit nur einem Klick bewerben',
                    style: TextStyle(
                      fontWeight: FontWeight.normal,
                      fontSize: 15.sp,
                      color: const Color(0xff718096),
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: SvgPicture.asset(
                  Assets.featureThree,
                  fit: BoxFit.contain,
                  height: 300,
                  alignment: Alignment.topCenter,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
