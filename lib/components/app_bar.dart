import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:tomisha_test/provider.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomeAppBar extends StatelessWidget {
  const CustomeAppBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(colors: [
              Color(0xff319795),
              Color(0xff3182CE),
            ]),
          ),
          height: 5.h,
        ),
        Expanded(
          child: Container(
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(12),
                  bottomRight: Radius.circular(12)),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey,
                  offset: Offset(0.0, 1.0), //(x,y)
                  blurRadius: 6.0,
                ),
              ],
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                //TODO: Add registration button
                Consumer(builder: (context, ref, _) {
                  final buttonState = ref.watch(buttonStatusProvider);
                  return buttonState.when(
                    visible: () => Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        const Text(
                          'Jetzt Klicken',
                          style: TextStyle(
                            color: Color(0xff4A5568),
                          ),
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        Flexible(
                          child: ElevatedButton(
                            onPressed: () {},
                            style: ElevatedButton.styleFrom(
                                primary: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12.0)),
                                padding: const EdgeInsets.all(0)),
                            child: Ink(
                              decoration: BoxDecoration(
                                // color: Colors.transparent,
                                border: Border.all(
                                    color: const Color(0xffCBD5E0), width: 1),
                                borderRadius: const BorderRadius.all(
                                    Radius.circular(12.0)),
                              ),
                              child: Container(
                                constraints: const BoxConstraints(
                                  maxHeight: 36.0,
                                  minWidth: 180,
                                ),
                                alignment: Alignment.center,
                                child: const Text(
                                  'Kostenlos Registrieren',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Color(0xff319795),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    notVisible: () => Container(),
                  );
                }),
                // const SizedBox(
                //   width: 8,
                // ),
                //Login Button
                TextButton(
                  onPressed: () {},
                  style: TextButton.styleFrom(
                    primary: const Color(0xff319795),
                  ),
                  child: const Text('Login'),
                ),
                const SizedBox(
                  width: 10,
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
