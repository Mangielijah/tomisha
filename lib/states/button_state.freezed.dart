// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'button_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$RegButtonState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() visible,
    required TResult Function() notVisible,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? visible,
    TResult Function()? notVisible,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? visible,
    TResult Function()? notVisible,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(VisibleBTN value) visible,
    required TResult Function(InVisibleBTN value) notVisible,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(VisibleBTN value)? visible,
    TResult Function(InVisibleBTN value)? notVisible,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(VisibleBTN value)? visible,
    TResult Function(InVisibleBTN value)? notVisible,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RegButtonStateCopyWith<$Res> {
  factory $RegButtonStateCopyWith(
          RegButtonState value, $Res Function(RegButtonState) then) =
      _$RegButtonStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$RegButtonStateCopyWithImpl<$Res>
    implements $RegButtonStateCopyWith<$Res> {
  _$RegButtonStateCopyWithImpl(this._value, this._then);

  final RegButtonState _value;
  // ignore: unused_field
  final $Res Function(RegButtonState) _then;
}

/// @nodoc
abstract class _$$VisibleBTNCopyWith<$Res> {
  factory _$$VisibleBTNCopyWith(
          _$VisibleBTN value, $Res Function(_$VisibleBTN) then) =
      __$$VisibleBTNCopyWithImpl<$Res>;
}

/// @nodoc
class __$$VisibleBTNCopyWithImpl<$Res>
    extends _$RegButtonStateCopyWithImpl<$Res>
    implements _$$VisibleBTNCopyWith<$Res> {
  __$$VisibleBTNCopyWithImpl(
      _$VisibleBTN _value, $Res Function(_$VisibleBTN) _then)
      : super(_value, (v) => _then(v as _$VisibleBTN));

  @override
  _$VisibleBTN get _value => super._value as _$VisibleBTN;
}

/// @nodoc

class _$VisibleBTN implements VisibleBTN {
  const _$VisibleBTN();

  @override
  String toString() {
    return 'RegButtonState.visible()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$VisibleBTN);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() visible,
    required TResult Function() notVisible,
  }) {
    return visible();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? visible,
    TResult Function()? notVisible,
  }) {
    return visible?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? visible,
    TResult Function()? notVisible,
    required TResult orElse(),
  }) {
    if (visible != null) {
      return visible();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(VisibleBTN value) visible,
    required TResult Function(InVisibleBTN value) notVisible,
  }) {
    return visible(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(VisibleBTN value)? visible,
    TResult Function(InVisibleBTN value)? notVisible,
  }) {
    return visible?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(VisibleBTN value)? visible,
    TResult Function(InVisibleBTN value)? notVisible,
    required TResult orElse(),
  }) {
    if (visible != null) {
      return visible(this);
    }
    return orElse();
  }
}

abstract class VisibleBTN implements RegButtonState {
  const factory VisibleBTN() = _$VisibleBTN;
}

/// @nodoc
abstract class _$$InVisibleBTNCopyWith<$Res> {
  factory _$$InVisibleBTNCopyWith(
          _$InVisibleBTN value, $Res Function(_$InVisibleBTN) then) =
      __$$InVisibleBTNCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InVisibleBTNCopyWithImpl<$Res>
    extends _$RegButtonStateCopyWithImpl<$Res>
    implements _$$InVisibleBTNCopyWith<$Res> {
  __$$InVisibleBTNCopyWithImpl(
      _$InVisibleBTN _value, $Res Function(_$InVisibleBTN) _then)
      : super(_value, (v) => _then(v as _$InVisibleBTN));

  @override
  _$InVisibleBTN get _value => super._value as _$InVisibleBTN;
}

/// @nodoc

class _$InVisibleBTN implements InVisibleBTN {
  const _$InVisibleBTN();

  @override
  String toString() {
    return 'RegButtonState.notVisible()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InVisibleBTN);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() visible,
    required TResult Function() notVisible,
  }) {
    return notVisible();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? visible,
    TResult Function()? notVisible,
  }) {
    return notVisible?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? visible,
    TResult Function()? notVisible,
    required TResult orElse(),
  }) {
    if (notVisible != null) {
      return notVisible();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(VisibleBTN value) visible,
    required TResult Function(InVisibleBTN value) notVisible,
  }) {
    return notVisible(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(VisibleBTN value)? visible,
    TResult Function(InVisibleBTN value)? notVisible,
  }) {
    return notVisible?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(VisibleBTN value)? visible,
    TResult Function(InVisibleBTN value)? notVisible,
    required TResult orElse(),
  }) {
    if (notVisible != null) {
      return notVisible(this);
    }
    return orElse();
  }
}

abstract class InVisibleBTN implements RegButtonState {
  const factory InVisibleBTN() = _$InVisibleBTN;
}
