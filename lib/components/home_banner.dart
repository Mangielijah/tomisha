import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tomisha_test/components/registration_button.dart';
import 'package:tomisha_test/utils.dart';

class HomeBanner extends StatelessWidget {
  const HomeBanner({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return ClipPath(
      clipper: WaveClipperTwo(),
      child: Container(
        height: size.width * 0.3,
        width: double.infinity,
        decoration: const BoxDecoration(
            gradient: LinearGradient(
          colors: [
            Color(0xffEBF4FF),
            Color(0xffE6FFFA),
          ],
        )),
        child: const BannerContent(),
      ),
    );
  }
}

class BannerContent extends StatelessWidget {
  const BannerContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double boxHeight = (size.width * 0.2);
    return SizedBox(
      width: double.infinity,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            width: size.height * 0.3,
            height: size.height * 0.3,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Deine Job Website',
                  style: TextStyle(
                    fontSize: 37.5.sp,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 1.95,
                  ),
                ),
                SizedBox(
                  height: 30.h,
                ),
                const RegistrationButton()
              ],
            ),
          ),
          const SizedBox(
            width: 100,
          ),
          SizedBox(
            width: 2 * (boxHeight / 1.5),
            height: 2 * (boxHeight / 1.5),
            child: Stack(
              children: [
                CircleAvatar(
                  backgroundColor: Colors.white,
                  radius: boxHeight / 1.75,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(boxHeight),
                    child: Container(
                      // margin: const EdgeInsets.all(28),
                      child: SvgPicture.asset(
                        Assets.handGreeting,
                        width: 1.95 * (boxHeight / 1.5),
                        fit: BoxFit.cover,
                        alignment: Alignment.topCenter,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
