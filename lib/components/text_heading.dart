import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tomisha_test/provider.dart';

class TextBanner extends ConsumerWidget {
  const TextBanner({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    Size size = MediaQuery.of(context).size;
    final feature = ref.watch(featureProvider);
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
            // height: 96,
            width: size.height * 0.35,
            child: feature.when(
              displayOne: () => Text(
                'Drei einfache Schritte zu deinem neuen Job',
                style: TextStyle(
                  fontSize: 22.sp,
                  fontWeight: FontWeight.normal,
                  color: const Color(0xff4A5568),
                ),
                textAlign: TextAlign.center,
              ),
              displayTwo: () => Text(
                'Drei einfache Schritte zu deinem neuen Mitarbeiter',
                style: TextStyle(
                  fontSize: 22.sp,
                  fontWeight: FontWeight.normal,
                  color: const Color(0xff4A5568),
                ),
                textAlign: TextAlign.center,
              ),
              displayThree: () => Text(
                'Drei einfache Schritte zur Vermittlung neuer Mitarbeiter',
                style: TextStyle(
                  fontSize: 22.sp,
                  fontWeight: FontWeight.normal,
                  color: const Color(0xff4A5568),
                ),
                textAlign: TextAlign.center,
              ),
            )),
      ],
    );
  }
}
