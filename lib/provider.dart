import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:tomisha_test/states/button_notifier.dart';
import 'package:tomisha_test/states/button_state.dart';
import 'package:tomisha_test/states/feature_notifier.dart';
import 'package:tomisha_test/states/feature_state.dart';

final buttonStatusProvider =
    StateNotifierProvider<ButtonWidgetNotifier, RegButtonState>(
  ((ref) => ButtonWidgetNotifier()),
);

final featureProvider = StateNotifierProvider<FeatureNotifier, FeatureState>(
  ((ref) => FeatureNotifier()),
);
