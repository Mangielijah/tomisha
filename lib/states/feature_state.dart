import 'package:freezed_annotation/freezed_annotation.dart';

part 'feature_state.freezed.dart';

@freezed
class FeatureState with _$FeatureState {
  const factory FeatureState.displayOne() = DisplayOne;
  const factory FeatureState.displayTwo() = DisplayTwo;
  const factory FeatureState.displayThree() = DisplayThree;
}
