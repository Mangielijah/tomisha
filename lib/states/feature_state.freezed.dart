// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'feature_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$FeatureState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() displayOne,
    required TResult Function() displayTwo,
    required TResult Function() displayThree,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? displayOne,
    TResult Function()? displayTwo,
    TResult Function()? displayThree,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? displayOne,
    TResult Function()? displayTwo,
    TResult Function()? displayThree,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(DisplayOne value) displayOne,
    required TResult Function(DisplayTwo value) displayTwo,
    required TResult Function(DisplayThree value) displayThree,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(DisplayOne value)? displayOne,
    TResult Function(DisplayTwo value)? displayTwo,
    TResult Function(DisplayThree value)? displayThree,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(DisplayOne value)? displayOne,
    TResult Function(DisplayTwo value)? displayTwo,
    TResult Function(DisplayThree value)? displayThree,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FeatureStateCopyWith<$Res> {
  factory $FeatureStateCopyWith(
          FeatureState value, $Res Function(FeatureState) then) =
      _$FeatureStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$FeatureStateCopyWithImpl<$Res> implements $FeatureStateCopyWith<$Res> {
  _$FeatureStateCopyWithImpl(this._value, this._then);

  final FeatureState _value;
  // ignore: unused_field
  final $Res Function(FeatureState) _then;
}

/// @nodoc
abstract class _$$DisplayOneCopyWith<$Res> {
  factory _$$DisplayOneCopyWith(
          _$DisplayOne value, $Res Function(_$DisplayOne) then) =
      __$$DisplayOneCopyWithImpl<$Res>;
}

/// @nodoc
class __$$DisplayOneCopyWithImpl<$Res> extends _$FeatureStateCopyWithImpl<$Res>
    implements _$$DisplayOneCopyWith<$Res> {
  __$$DisplayOneCopyWithImpl(
      _$DisplayOne _value, $Res Function(_$DisplayOne) _then)
      : super(_value, (v) => _then(v as _$DisplayOne));

  @override
  _$DisplayOne get _value => super._value as _$DisplayOne;
}

/// @nodoc

class _$DisplayOne implements DisplayOne {
  const _$DisplayOne();

  @override
  String toString() {
    return 'FeatureState.displayOne()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$DisplayOne);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() displayOne,
    required TResult Function() displayTwo,
    required TResult Function() displayThree,
  }) {
    return displayOne();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? displayOne,
    TResult Function()? displayTwo,
    TResult Function()? displayThree,
  }) {
    return displayOne?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? displayOne,
    TResult Function()? displayTwo,
    TResult Function()? displayThree,
    required TResult orElse(),
  }) {
    if (displayOne != null) {
      return displayOne();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(DisplayOne value) displayOne,
    required TResult Function(DisplayTwo value) displayTwo,
    required TResult Function(DisplayThree value) displayThree,
  }) {
    return displayOne(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(DisplayOne value)? displayOne,
    TResult Function(DisplayTwo value)? displayTwo,
    TResult Function(DisplayThree value)? displayThree,
  }) {
    return displayOne?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(DisplayOne value)? displayOne,
    TResult Function(DisplayTwo value)? displayTwo,
    TResult Function(DisplayThree value)? displayThree,
    required TResult orElse(),
  }) {
    if (displayOne != null) {
      return displayOne(this);
    }
    return orElse();
  }
}

abstract class DisplayOne implements FeatureState {
  const factory DisplayOne() = _$DisplayOne;
}

/// @nodoc
abstract class _$$DisplayTwoCopyWith<$Res> {
  factory _$$DisplayTwoCopyWith(
          _$DisplayTwo value, $Res Function(_$DisplayTwo) then) =
      __$$DisplayTwoCopyWithImpl<$Res>;
}

/// @nodoc
class __$$DisplayTwoCopyWithImpl<$Res> extends _$FeatureStateCopyWithImpl<$Res>
    implements _$$DisplayTwoCopyWith<$Res> {
  __$$DisplayTwoCopyWithImpl(
      _$DisplayTwo _value, $Res Function(_$DisplayTwo) _then)
      : super(_value, (v) => _then(v as _$DisplayTwo));

  @override
  _$DisplayTwo get _value => super._value as _$DisplayTwo;
}

/// @nodoc

class _$DisplayTwo implements DisplayTwo {
  const _$DisplayTwo();

  @override
  String toString() {
    return 'FeatureState.displayTwo()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$DisplayTwo);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() displayOne,
    required TResult Function() displayTwo,
    required TResult Function() displayThree,
  }) {
    return displayTwo();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? displayOne,
    TResult Function()? displayTwo,
    TResult Function()? displayThree,
  }) {
    return displayTwo?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? displayOne,
    TResult Function()? displayTwo,
    TResult Function()? displayThree,
    required TResult orElse(),
  }) {
    if (displayTwo != null) {
      return displayTwo();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(DisplayOne value) displayOne,
    required TResult Function(DisplayTwo value) displayTwo,
    required TResult Function(DisplayThree value) displayThree,
  }) {
    return displayTwo(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(DisplayOne value)? displayOne,
    TResult Function(DisplayTwo value)? displayTwo,
    TResult Function(DisplayThree value)? displayThree,
  }) {
    return displayTwo?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(DisplayOne value)? displayOne,
    TResult Function(DisplayTwo value)? displayTwo,
    TResult Function(DisplayThree value)? displayThree,
    required TResult orElse(),
  }) {
    if (displayTwo != null) {
      return displayTwo(this);
    }
    return orElse();
  }
}

abstract class DisplayTwo implements FeatureState {
  const factory DisplayTwo() = _$DisplayTwo;
}

/// @nodoc
abstract class _$$DisplayThreeCopyWith<$Res> {
  factory _$$DisplayThreeCopyWith(
          _$DisplayThree value, $Res Function(_$DisplayThree) then) =
      __$$DisplayThreeCopyWithImpl<$Res>;
}

/// @nodoc
class __$$DisplayThreeCopyWithImpl<$Res>
    extends _$FeatureStateCopyWithImpl<$Res>
    implements _$$DisplayThreeCopyWith<$Res> {
  __$$DisplayThreeCopyWithImpl(
      _$DisplayThree _value, $Res Function(_$DisplayThree) _then)
      : super(_value, (v) => _then(v as _$DisplayThree));

  @override
  _$DisplayThree get _value => super._value as _$DisplayThree;
}

/// @nodoc

class _$DisplayThree implements DisplayThree {
  const _$DisplayThree();

  @override
  String toString() {
    return 'FeatureState.displayThree()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$DisplayThree);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() displayOne,
    required TResult Function() displayTwo,
    required TResult Function() displayThree,
  }) {
    return displayThree();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? displayOne,
    TResult Function()? displayTwo,
    TResult Function()? displayThree,
  }) {
    return displayThree?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? displayOne,
    TResult Function()? displayTwo,
    TResult Function()? displayThree,
    required TResult orElse(),
  }) {
    if (displayThree != null) {
      return displayThree();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(DisplayOne value) displayOne,
    required TResult Function(DisplayTwo value) displayTwo,
    required TResult Function(DisplayThree value) displayThree,
  }) {
    return displayThree(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(DisplayOne value)? displayOne,
    TResult Function(DisplayTwo value)? displayTwo,
    TResult Function(DisplayThree value)? displayThree,
  }) {
    return displayThree?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(DisplayOne value)? displayOne,
    TResult Function(DisplayTwo value)? displayTwo,
    TResult Function(DisplayThree value)? displayThree,
    required TResult orElse(),
  }) {
    if (displayThree != null) {
      return displayThree(this);
    }
    return orElse();
  }
}

abstract class DisplayThree implements FeatureState {
  const factory DisplayThree() = _$DisplayThree;
}
