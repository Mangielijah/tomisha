class Assets {
  static const String handGreeting = 'assets/undraw_agreement_aajr.svg';
  static const String featureOne = 'assets/undraw_Profile_data_re_v81r.svg';
  static const String featureTwo = 'assets/undraw_task_31wc.svg';
  static const String downArrowRight = 'assets/Gruppe 1821.svg';
  static const String downArrowLeft = 'assets/Gruppe 1822.svg';
  static const String featureThree = 'assets/undraw_personal_file_222m.svg';
  static const String featureTwoD3 = 'assets/undraw_job_offers_kw5d.svg';
  static const String featureTwoD2 = 'assets/undraw_about_me_wa29.svg';
  static const String featureThreeD2 = 'assets/undraw_swipe_profiles1_i6mr.svg';
  static const String featureThreeD3 = 'assets/undraw_business_deal_cpi9.svg';
}
