import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tomisha_test/provider.dart';
import 'package:tomisha_test/utils.dart';

class MobileFeatureDisplay extends StatelessWidget {
  const MobileFeatureDisplay({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: 1300,
      width: size.width,
      color: Colors.white,
      child: Stack(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: 20.h,
              ),
              const MobileFeatureOne(),
              SizedBox(
                height: 4.h,
              ),
              const MobileFeatureTwo(),
              const MobileFeatureThree()
            ],
          ),
        ],
      ),
    );
  }
}

class MobileFeatureOne extends ConsumerWidget {
  const MobileFeatureOne({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final feature = ref.watch(featureProvider);
    return SizedBox(
      height: 269,
      child: Stack(
        children: [
          Positioned(
            right: 50,
            child: SvgPicture.asset(
              Assets.featureOne,
              height: 150.h,
              fit: BoxFit.cover,
              alignment: Alignment.topCenter,
            ),
          ),
          Align(
            alignment: Alignment.bottomLeft,
            child: SizedBox(
              height: 156,
              // color: Colors.red,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Container(
                    margin: const EdgeInsets.only(left: 8),
                    child: Text(
                      '1.',
                      style: TextStyle(
                        fontSize: 110.sp,
                        color: const Color(0xff718096),
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Flexible(
                    child: Container(
                      margin: const EdgeInsets.only(bottom: 26),
                      width: 275,
                      child: feature.when(
                        displayOne: () => Text(
                          'Erstellen dein Lebenslauf',
                          style: TextStyle(
                            fontWeight: FontWeight.normal,
                            fontSize: 19.sp,
                            color: const Color(0xff718096),
                          ),
                        ),
                        displayTwo: () => Text(
                          'Erstellen dein Unternehmensprofil',
                          style: TextStyle(
                            fontWeight: FontWeight.normal,
                            fontSize: 19.sp,
                            color: const Color(0xff718096),
                          ),
                        ),
                        displayThree: () => Text(
                          'Erstellen dein Unternehmensprofil',
                          style: TextStyle(
                            fontWeight: FontWeight.normal,
                            fontSize: 19.sp,
                            color: const Color(0xff718096),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

class MobileFeatureTwo extends ConsumerWidget {
  const MobileFeatureTwo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final feature = ref.watch(featureProvider);
    return SizedBox(
      height: 400,
      child: Stack(
        children: [
          Align(
            alignment: Alignment.topLeft,
            child: ClipPath(
              clipper: WaveClipperOne(
                reverse: true,
                flip: true,
              ),
              child: Container(
                height: 200,
                width: double.infinity,
                decoration: const BoxDecoration(
                    gradient: LinearGradient(
                  colors: [
                    Color(0xffE6FFFA),
                    Color(0xffEBF4FF),
                  ],
                )),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomLeft,
            child: ClipPath(
              clipper: WaveClipperOne(),
              child: Container(
                height: 200,
                width: double.infinity,
                decoration: const BoxDecoration(
                    gradient: LinearGradient(
                  colors: [
                    Color(0xffE6FFFA),
                    Color(0xffEBF4FF),
                  ],
                )),
              ),
            ),
          ),
          Positioned(
            right: 50,
            bottom: 50,
            child: feature.when(
              displayOne: () => SvgPicture.asset(
                Assets.featureTwo,
                height: 127.h,
                fit: BoxFit.cover,
                alignment: Alignment.topCenter,
              ),
              displayTwo: () => SvgPicture.asset(
                Assets.featureTwoD2,
                height: 127.h,
                fit: BoxFit.cover,
                alignment: Alignment.topCenter,
              ),
              displayThree: () => SvgPicture.asset(
                Assets.featureTwoD3,
                height: 127.h,
                fit: BoxFit.cover,
                alignment: Alignment.topCenter,
              ),
            ),
          ),
          Positioned(
            top: 30,
            child: SizedBox(
              height: 156,
              // color: Colors.red,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Container(
                    margin: const EdgeInsets.only(left: 8),
                    child: Text(
                      '2.',
                      style: TextStyle(
                        fontSize: 110.sp,
                        color: const Color(0xff718096),
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 30,
                  ),
                  Container(
                    margin: const EdgeInsets.only(bottom: 26),
                    width: 275,
                    child: feature.when(
                      displayOne: () => Text(
                        'Erstellen dein Lebenslauf',
                        style: TextStyle(
                          fontWeight: FontWeight.normal,
                          fontSize: 19.sp,
                          color: const Color(0xff718096),
                        ),
                      ),
                      displayTwo: () => Text(
                        'Erstellen ein Jobinserat',
                        style: TextStyle(
                          fontWeight: FontWeight.normal,
                          fontSize: 19.sp,
                          color: const Color(0xff718096),
                        ),
                      ),
                      displayThree: () => Text(
                        'Erhalte Vermittlungs- angebot von Arbeitgeber',
                        style: TextStyle(
                          fontWeight: FontWeight.normal,
                          fontSize: 19.sp,
                          color: const Color(0xff718096),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

class MobileFeatureThree extends ConsumerWidget {
  const MobileFeatureThree({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final feature = ref.watch(featureProvider);
    return SizedBox(
      height: 400,
      child: Stack(
        children: [
          const Positioned(
            left: -80,
            top: -50,
            child: CircleAvatar(
              radius: 180,
              backgroundColor: Color(0xffF7FAFC),
            ),
          ),
          Positioned(
            right: 50,
            bottom: 50,
            child: feature.when(
              displayOne: () => SvgPicture.asset(
                Assets.featureThree,
                height: 127.h,
                fit: BoxFit.cover,
                alignment: Alignment.topCenter,
              ),
              displayTwo: () => SvgPicture.asset(
                Assets.featureThreeD2,
                height: 127.h,
                fit: BoxFit.cover,
                alignment: Alignment.topCenter,
              ),
              displayThree: () => SvgPicture.asset(
                Assets.featureThreeD3,
                height: 127.h,
                fit: BoxFit.cover,
                alignment: Alignment.topCenter,
              ),
            ),
          ),
          Positioned(
            top: 0,
            child: SizedBox(
              height: 156,
              // color: Colors.red,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Container(
                    margin: const EdgeInsets.only(left: 8),
                    child: Text(
                      '3.',
                      style: TextStyle(
                        fontSize: 110.sp,
                        color: const Color(0xff718096),
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 30,
                  ),
                  Container(
                    margin: const EdgeInsets.only(bottom: 26),
                    width: 275,
                    child: feature.when(
                      displayOne: () => Text(
                        'Mit nur einem Klick bewerben',
                        style: TextStyle(
                          fontWeight: FontWeight.normal,
                          fontSize: 19.sp,
                          color: const Color(0xff718096),
                        ),
                      ),
                      displayTwo: () => Text(
                        'Wähle deinen neuen Mitarbeiter aus',
                        style: TextStyle(
                          fontWeight: FontWeight.normal,
                          fontSize: 19.sp,
                          color: const Color(0xff718096),
                        ),
                      ),
                      displayThree: () => Text(
                        'Vermittlung nach Provision oder Stundenlohn',
                        style: TextStyle(
                          fontWeight: FontWeight.normal,
                          fontSize: 19.sp,
                          color: const Color(0xff718096),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
