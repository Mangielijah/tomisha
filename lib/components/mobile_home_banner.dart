import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tomisha_test/utils.dart';

class MobileHomeBanner extends StatelessWidget {
  const MobileHomeBanner({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      height: 590,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Color(0xffEBF4FF),
            Color(0xffE6FFFA),
          ],
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
            child: Text(
              'Deine Job Website',
              style: TextStyle(
                fontSize: 37.5.sp,
                fontWeight: FontWeight.bold,
                letterSpacing: 1.95,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          Expanded(
            child: SizedBox(
              // height: double.infinity,
              child: SvgPicture.asset(
                Assets.handGreeting,
                fit: BoxFit.cover,
              ),
            ),
          )
        ],
      ),
    );
  }
}
