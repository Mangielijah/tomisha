import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:tomisha_test/states/feature_state.dart';

class FeatureNotifier extends StateNotifier<FeatureState> {
  FeatureNotifier() : super(const FeatureState.displayOne());

  void toggleFeature(int index) {
    late FeatureState newState;
    if (index == 0) {
      newState = const DisplayOne();
    } else if (index == 1) {
      newState = const DisplayTwo();
    } else {
      newState = const DisplayThree();
    }
    state = newState;
    // state.when(
    //   displayOne: () {
    //     state = const FeatureState.notVisible();
    //   },
    //   displayTwo: () {
    //     state = const FeatureState.visible();
    //   },
    //   displayThree: () {},
    // );
  }
}
